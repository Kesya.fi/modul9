public class Baju extends Pakaian{
	// Kode Warna R=Merah, B=Biru, G=Hijau, U=Belum ditentukan
	public char kodeWarna = 'U';
	public double harga = 0;
	public void setKodeWarna(char kodeWarna){
		this.kodeWarna = kodeWarna;
	}
	public char getKodeWarna(){
		return kodeWarna;
	}
	public void setHargaBaju(double harga){
		this.harga = harga;
	}
	public double getHargaBaju(){
		return harga;
	}

	// Method ini menampilkan nilai untuk suatu suatu item
	public void tampilInformasiBaju(){
	System.out.println("ID Baju	: " +getID());
	System.out.println("Keterangan	: " +getKeterangan());
	System.out.println("Kode Warna	: " +getKodeWarna());
	System.out.println("Harga Baju	: " +getHargaBaju());
	System.out.println("Jumlah Stok	: " +getJmlStock());
	} // Akhir method display
} // Akhir kelas
