public class Pakaian {
	private int ID = 0; // Default_ID untuk semua pakaian
	private String keterangan = "-keterangan diperlukan-"; // default 

	
	private double harga = 0.0; // Harga default untuk semua pakaian 
	private int jmlStok = 0; //jumlah default untuk semua pakaian
	
	private static int UNIQUE_ID=0; // Static member ditambahkan dalam constructor // untuk menghasilkan ID yang unik
	
	public Pakaian() {
		ID=UNIQUE_ID++;
	}
	public int getID() {
		return ID;
	}
	public void setKeterangan (String d){
		keterangan=d;
	}
	public String getKeterangan(){
		return keterangan;
	}
	public void setHarga (double p) {
		harga = p;
	}
	public double getHarga() {
		return harga;
	}
	public void setJmlStock (int q){
		jmlStok=q;
	}
	public int getJmlStock(){
		return jmlStok;
	}
}