public abstract class LivingThing{
	public void breath(){
		System.out.println("Living Thing breathing..."); 
	}
	public void eat(){
		System.out.println("Living Thing eating...");
	}
	/**
	* abstrack method walk
	* kita ingin method ini di-overridden oleh subclass
	**/
	public abstract void walk();
}
